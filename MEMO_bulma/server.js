var express = require('express');
const ejs = require('ejs');
var app = express();
var fs = require('fs');
const Sequelize = require('sequelize');
var path = require('path');
app.use(express.static(path.join(__dirname, 'client')));
app.use(express.static(path.join(__dirname, 'views')));

app.set('view engine', 'ejs');
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// db connection
let sequelize = new Sequelize('memo', 'memo', 'memo@2020', {
    host: 'localhost',
    dialect: 'mariadb',
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
});

// sync models
const Memo = sequelize.import(__dirname + '/memo');
Memo.sync();
//end connection 


////inizio a creare il progetto REST/////
app.get('/', (req, res) => { 
    res.render('index') ;
});

app.get('/aggiungi', function (req, res) {
    Memo.findAll().then(memos => {
        res.json(memos);
    })
});

app.post('/aggiungi', function (req, res) {
    var productName = req.body.name;
    var productName1 = req.body.title;
    Memo.create({ title:productName1,name: productName }).then(memo => {
        res.json({ msg: `memo: ${memo} created` });
    })
});

app.put('/aggiorna/:id', function (req, res) {
    var id = req.params.id;
    var newName = req.body.newName;
    var newTitle = req.body.newTitle;


    Memo.update({
        title: newTitle,
        name: newName

    },
        {
            where: {
                id: id
            }
        }).then(
            res.send("aggiornato")
        )

});

app.delete('/elimina/:id', function (req, res) {
    var id = req.params.id;
    Memo.destroy({
        where: {
            id: id
        }
    }).then(
        res.send('Successfully deleted product!')
    )

});

app.listen(3000, function () {
    console.log('Server listening on ' + 3000);
});
