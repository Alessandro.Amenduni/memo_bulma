$(document).ready(function() {
  mostra();

   $("#bottone").click(function(){
      var product = $("#aggiungi").val(); 
      var product1 = $("#intestazione").val();
      $.ajax({
        url: "/aggiungi",
        type: "POST",
        contentType: "application/json",
        data:JSON.stringify({
          title:product1,  
          name: product, 
        }),
        success: function(risposta){
         mostra();
      },
       error: function(){
       alert("Chiamata fallita!!!");
    }
    })
  });
});

function mostra(){
  $.ajax({
    url: "/aggiungi",
    contentType: "application/json",
      success: function(risposta) {
        $("#risposta").html(' ');
        risposta.forEach(function(ris) {
          $("#risposta").append('<article  class="message is-warning"><div class="message-header"><p>'+ ris.title +'</p><button class="update is-right" onclick="aggiorna('+ris.id+')" aria-label="Update"></button><button class="delete" onclick="cancella('+ris.id+')" aria-label="delete"></button></div><div class="message-body">'+ ris.name +'</div></article>');
        });
    }
    });
};

  function cancella(id){
  $.ajax({
    url: "/elimina/" +id,
    type: "DELETE",
    success: function(risposta){
     mostra();
  },
   error: function(){
   alert("Chiamata fallita!!!");
}
})
  }

  function aggiorna(id){
    var newTitle = $("#intestazione").val();
    var newName = $("#aggiungi").val();
    $.ajax({
      url: "/aggiorna/"+id,
      type: "PUT",
      contentType: "application/json",
      data:JSON.stringify({newName:newName, newTitle:newTitle}),
      success: function(risposta){
        mostra();
    },
     error: function(){
     alert("Chiamata fallita!!!");
  }	
  })
    }


