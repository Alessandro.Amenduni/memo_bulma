module.exports = (sequelize, DataTypes) => {
    const Memo = sequelize.define('memo', {
        title: {type: DataTypes.STRING(20), unique: true}, 
        name: DataTypes.TEXT
      },
      {
        freezeTableName: true,
      }
    );
    return Memo;
}

